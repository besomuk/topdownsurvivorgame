﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public Text txtEnemiesSpawned;
    public Text txtPlayerInfoHealth;
    void Start()
    {
        if ( instance == null )
            instance = this;
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Init()
    {
        txtEnemiesSpawned.text = "...";
    }

    public void UpdateUI()
    {
        txtEnemiesSpawned.text = GameManager.enemiesSpawned.ToString();
        txtPlayerInfoHealth.text = PlayerController.instance.player.Health.ToString();
    }
}
