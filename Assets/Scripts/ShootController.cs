﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] GameObject bulletSpawn;
    [SerializeField] float bulletSpeed = 600;
    [SerializeField] int maximumBullets = 10;
    public bool canShoot = true;

    [SerializeField] int bulletCounter = 0;

    Transform t;

    void Start()
    {
        t = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetMouseButtonDown(0) && GameManager.totalBulletsOnScreen < maximumBullets )
        {
            Shoot();
        }
    }

    void Shoot()
    {
        Debug.Log( "Fire!" );
        GameObject newBullet = Instantiate( bulletPrefab, bulletSpawn.transform.position, Quaternion.identity );
        newBullet.GetComponent<Rigidbody>().AddForce( gameObject.transform.forward * bulletSpeed );
        newBullet.tag = "bullet";
        GameManager.totalBulletsOnScreen++;
    }
}
