﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] Camera camera;
    [SerializeField] Transform targetObject;

    private Vector3 cameraPosition;
    [SerializeField] private Vector3 cameraOffset = new Vector3( 0, 50, -40 );

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 newCameraPosition = new Vector3( target.transform.position.x + cameraOffset.x, target.transform.position.y + cameraOffset.y, target.transform.position.z + cameraOffset.z );

        camera.transform.position = CalculateNewCameraPosition( targetObject.transform.position );
    }

    Vector3 CalculateNewCameraPosition ( Vector3 target )
    {
        Vector3 finalPosition;
        finalPosition = new Vector3( target.x + cameraOffset.x, target.y + cameraOffset.y, target.z + cameraOffset.z );
        return finalPosition;
    }

}
