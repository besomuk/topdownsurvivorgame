﻿using System.Collections;
using System.Collections.Generic;

public class Player
{
    private int health;

    public Player( int _health )
    {
        health = _health;
    }

    public int Health
    {
        get { return health; }
        set { health = value; }
    }

    public void Damage ( int _dmg )
    {
        health -= _dmg;
    }

    public void Heal( int _amnt )
    {
        health += _amnt;
    }
}