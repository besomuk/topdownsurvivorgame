﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public int power  = 5;
    public int health = 100;

    public Enemy enemy;

    void Start()
    {
        enemy = new Enemy( 50 );
        health = enemy.Health;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter( Collider other )
    {
        if ( other.tag == "bullet" )
        {
            Destroy( gameObject );
            GameManager.enemiesSpawned--;
            Global.ui.UpdateUI();
        }

        if ( other.tag == "Player" )
        {
            Destroy( gameObject );
        }
    }
}
