﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour
{
    public static UIManager ui;
    public static Tools tools;
    public static Player player;

    void Start()
    {
        ui    = GetComponent<UIManager>();
        tools = GetComponent<Tools>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
