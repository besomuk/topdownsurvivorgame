using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    [SerializeField] private float rotationSpeed = 200;           // rotation speed
    [SerializeField] private float moveSpeed = 6;                 // movement speed
    private float speedMod = 1;                                   // speed modificator
    [SerializeField] private float speedModFactor = 2;            // speed increase with SHIFT. New speed is moveSpeed * speedModFactor
    [Tooltip("If on, mouse rotation script should be disabled!")]
    [SerializeField] private bool strafeOn = false;

    public Player player;

    void Start()
    {
        Debug.Log( "Start..." );

        if ( !instance ) instance = this;

        player = new Player( 100 );

    }
    void Update()
    {
        /*
        if ( strafeOn )
        {
            Strafe();
        }
        else
        {
            Rotate();
        }
        //Move();
        */

        Move2();
    }

    public void OnTriggerEnter( Collider other )
    {
        if ( other.tag == "enemy1" )
        {
            int dmgAmount = other.GetComponent<EnemyScript>().power;
            player.Damage( dmgAmount );
            Debug.Log( player.Health );
            Global.ui.UpdateUI();
        }
    }

    /* Better move */
    private void Move2()
    {
        if ( Input.GetKey( KeyCode.W ) )
        {
            transform.Translate( new Vector3 ( 0, 0, 1 ) * Time.deltaTime * moveSpeed * speedMod, Space.World);
        }
        if ( Input.GetKey( KeyCode.S ) )
        {
            transform.Translate( new Vector3 ( 0, 0, -1 ) * Time.deltaTime * moveSpeed * speedMod, Space.World);
        }
        if ( Input.GetKey( KeyCode.A ) )
        {
            transform.Translate( new Vector3( -1, 0, 0 ) * Time.deltaTime * moveSpeed * speedMod, Space.World );
        }
        if ( Input.GetKey( KeyCode.D ) )
        {
            transform.Translate( new Vector3( 1, 0, 0 ) * Time.deltaTime * moveSpeed * speedMod, Space.World );
        }

        if ( Input.GetKeyDown ( KeyCode.LeftShift ))
        {
            speedMod = speedModFactor;
        }
        if ( Input.GetKeyUp ( KeyCode.LeftShift ) )
        {
            speedMod = 1;
        }

    }

    /* Old move starts here ... */
    private void Move()
    {
        if ( Input.GetKey( KeyCode.W ) )
        {
            transform.Translate( Vector3.forward * Time.deltaTime * moveSpeed );
        }
        if ( Input.GetKey( KeyCode.S ) )
        {
            transform.Translate( Vector3.forward * Time.deltaTime * -moveSpeed );
        }
    }

    void Strafe ()
    {
        if ( Input.GetKey( KeyCode.A ) )
        {
            transform.Translate( Vector3.left * Time.deltaTime * moveSpeed );
        }
        if ( Input.GetKey( KeyCode.D ) )
        {
            transform.Translate( Vector3.right * Time.deltaTime * moveSpeed );
        }
    }

    void Rotate()
    {
        if ( Input.GetKey( KeyCode.A ) )
        {
            transform.Rotate( new Vector3( 0, 1, 0 ) * Time.deltaTime * -rotationSpeed );
        }
        if ( Input.GetKey( KeyCode.D ) )
        {
            transform.Rotate( new Vector3( 0, 1, 0 ) * Time.deltaTime * rotationSpeed );
        }
    }
    /* Old move ends here ... */
}
