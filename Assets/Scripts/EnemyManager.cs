﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;

    [SerializeField] int numberOfEnemies = 3;

    [SerializeField] string enemyTag = "enemy1";

    [Header( "Spawn limits" )]
    [SerializeField] GameObject enemySpawn;
    [SerializeField] float xOffset = 30f;
    [SerializeField] float zOffset = 30f;

    void Start()
    {
        for ( int i = 0; i < numberOfEnemies; i++ )
        {
            CreateEnemy();
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    void CreateEnemy ()
    {
        //Debug.Log( "Enemy created!" );
        Vector3 spawnPosition = Vector3.zero;

        float randomXPosition = Random.Range( enemySpawn.transform.position.x - xOffset, enemySpawn.transform.position.x + xOffset );
        float randomZPosition = Random.Range( enemySpawn.transform.position.z - zOffset, enemySpawn.transform.position.z + zOffset );

        spawnPosition = new Vector3( randomXPosition, 3.77f, randomZPosition );

        GameObject newEnemy = Instantiate( enemyPrefab, spawnPosition, Quaternion.identity );
        newEnemy.transform.SetParent( gameObject.transform );
        newEnemy.name = "Enemy";
        newEnemy.tag = enemyTag;

        GameManager.enemiesSpawned++;

        Global.ui.UpdateUI();
    }
}
