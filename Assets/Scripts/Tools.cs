﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tools : MonoBehaviour
{
    public static Tools instance;

    [SerializeField] KeyCode restartSceneKey = KeyCode.R;

    void Start()
    {
        if ( instance == null )
            instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKeyDown ( restartSceneKey ))
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().name );
        }
    }
}
