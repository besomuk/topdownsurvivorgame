﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavigationAgent : MonoBehaviour
{
    public Transform target;

    NavMeshAgent agent;

    void Start()
    {

        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if ( gameObject.tag != "enemy_proto" )
            agent.SetDestination( target.position );
    }
}
